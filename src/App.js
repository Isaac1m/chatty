import React, { useState } from "react";
import "./App.css";
import { Sidebar, Chat } from "./components";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Login } from "./components";
import { useStateValue } from "./context/StateProvider";

const App = () => {
  const [{ user }] = useStateValue();
  return (
    <Router>
      <div className="app">
        {!user ? (
          <Login />
        ) : (
          <div className="app__body">
            <Sidebar />
            <Routes>
              <Route path="/rooms/:roomId" element={<Chat />} />
            </Routes>
          </div>
        )}
      </div>
    </Router>
  );
};

export default App;
