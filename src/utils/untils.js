export const sortMessages = (messages) => {
  return messages.sort((a, b) => new Date(b.timestamp) - new Date(a.timestamp));
};
