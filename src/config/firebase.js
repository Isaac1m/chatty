import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { collection, addDoc } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyA3HIQfEs35oLgegcLU73yGZo9aN69y9nU",
  authDomain: "chatty-22a8c.firebaseapp.com",
  projectId: "chatty-22a8c",
  storageBucket: "chatty-22a8c.appspot.com",
  messagingSenderId: "36921212218",
  appId: "1:36921212218:web:33c71dd5a60941616ae403",
  measurementId: "G-E7Z6P3MTC2",
};

const addData = async (database, dbCollection, data) => {
  // Add a new document in collection @collection
  await addDoc(collection(database, dbCollection), {
    name: data,
  });
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();

export { auth, provider, addData };

export default db;
