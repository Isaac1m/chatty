import React, { useState, useEffect } from "react";
import { Avatar, IconButton } from "@mui/material";
import db from "../../config/firebase";
import "./Chat.css";
import {
  AttachFile,
  InsertEmoticon,
  Mic,
  SearchOutlined,
} from "@mui/icons-material";
import MoreVert from "@mui/icons-material/MoreVert";
import { useParams } from "react-router-dom";
import {
  doc,
  getDoc,
  getDocs,
  setDoc,
  collection,
  Timestamp,
} from "firebase/firestore";
import { useStateValue } from "../../context/StateProvider";

const Chat = () => {
  const [seed, setSeed] = useState("");
  const [input, setInput] = useState("");
  const { roomId } = useParams();
  const [roomName, setRoomName] = useState("");
  const [messages, setMessages] = useState([]);
  const [lastActive, setLastActive] = useState(null);
  const [{ user }] = useStateValue();

  useEffect(() => {
    async function querySnapshot() {
      setMessages([]);
      const messagesArr = [];
      if (roomId) {
        const docRef = doc(db, "chat-rooms", roomId);
        const docSnap = await getDoc(docRef);

        const messagesSnapshot = await getDocs(collection(docRef, "messages"));

        messagesSnapshot?.forEach((doc) => {
          messagesArr.push(doc.data());
        });

        setMessages(messagesArr);

        if (docSnap.exists()) {
          setRoomName(docSnap.data().name);
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }
    }

    querySnapshot();
  }, [roomId]);

  useEffect(() => {
    setSeed(Math.floor(Math.random() * 1200));
  }, [roomId]);

  useEffect(() => {
    setLastActive(
      messages.length > 0
        ? messages[0]?.timestamp?.toDate().toUTCString()
        : "Be the first To Post Here."
    );
  }, [messages]);

  const sendMessage = async (e) => {
    e.preventDefault();

    await setDoc(doc(collection(doc(db, "chat-rooms", roomId), "messages")), {
      message: input,
      postedBy: user.displayName,
      timestamp: Timestamp.now(),
    });
    setInput("");
  };
  return (
    <div className="chat">
      <div className="chat__header">
        <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
        <div className="chat__headerInfo">
          <h3>{roomName}</h3>
          <p>Last active: {lastActive && lastActive}</p>
        </div>
        <div className="chat__headerRight">
          <IconButton>
            <SearchOutlined />
          </IconButton>
          <IconButton>
            <AttachFile />
          </IconButton>
          <IconButton>
            <MoreVert />
          </IconButton>
        </div>
      </div>
      <div className="chat__body">
        {messages &&
          messages?.map((message, index) => (
            <p
              className={`chat__message ${
                message.postedBy === user.displayName && "chat__reciever"
              }`}
              key={index}
            >
              <span className="chat__creator">{message.postedBy}</span>
              {message.message}
              <span className="chat__timestamp">
                {message.timestamp?.toDate().toUTCString()}
              </span>
            </p>
          ))}
      </div>
      <div className="chat__footer">
        <InsertEmoticon />
        <form>
          <input
            value={input}
            type="text"
            placeholder="Type message..."
            onChange={(e) => setInput(e.target.value)}
          />
          <button type="submit" onClick={sendMessage}>
            Send
          </button>
        </form>
        <Mic />
      </div>
    </div>
  );
};

export default Chat;
