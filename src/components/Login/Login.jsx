import React from "react";
import { Button } from "@mui/material";
import { auth, provider } from "../../config/firebase";
import { signInWithPopup } from "firebase/auth";
import "./Login.css";
import { useStateValue } from "../../context/StateProvider";
import { actionTypes } from "../../context/reducer";

const Login = () => {
  const [{}, dispatch] = useStateValue();
  const signIn = (e) => {
    e.preventDefault();
    signInWithPopup(auth, provider)
      .then((result) => {
        // // const credential = GoogleAuthProvider.credentialFromResult(result);
        // console.log(result);
        dispatch({
          type: actionTypes.SET_USER,
          user: result.user,
        });
      })
      .catch((err) => {
        console.error(`Error signing in :${err}`);
      });
  };
  return (
    <div className="login">
      <div className="login__container">
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/1/19/WhatsApp_logo-color-vertical.svg"
          alt="WhatsApp logo"
        />
        <div className="login__text">
          <h1>Login</h1>
        </div>
        <Button onClick={signIn}>Sign in with Google</Button>
      </div>
    </div>
  );
};

export default Login;
