import { Avatar, IconButton } from "@mui/material";
import DonutLargeIcon from "@mui/icons-material/DonutLarge";
import ChatIcon from "@mui/icons-material/Chat";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { SearchOutlined } from "@mui/icons-material";
import React, { useState, useEffect } from "react";
import { collection, getDocs } from "firebase/firestore";
import { SidebarChat } from "../";
import db from "../../config/firebase";
import "./Sidebar.css";
import { useStateValue } from "../../context/StateProvider";

const Sidebar = () => {
  const [chatRooms, setChatRooms] = useState([]);
  const [{ user }] = useStateValue();

  useEffect(() => {
    async function querySnapshot() {
      const rooms = [];
      const snapshot = await getDocs(collection(db, "chat-rooms"));

      snapshot.forEach((doc) =>
        rooms.push({
          id: doc.id,
          data: doc.data(),
        })
      );

      setChatRooms(rooms);
    }
    querySnapshot();
  }, []);

  // console.log(chatRooms);
  return (
    <div className="sidebar">
      <div className="sidebar__header">
        <Avatar src={user?.photoURL} />
        <div className="sidebar__headerRight">
          <IconButton>
            <DonutLargeIcon />
          </IconButton>
          <IconButton>
            <ChatIcon />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
      </div>
      <div className="sidebar__search">
        <div className="sidebar__searchContainer">
          <SearchOutlined />
          <input type="text" placeholder="Search or start new chat" />
        </div>
      </div>
      <div className="sidebar__chats">
        <SidebarChat addNewChatRoom />
        {chatRooms?.map((chatRoom) => (
          <SidebarChat chatRoom={chatRoom} key={chatRoom.id} />
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
