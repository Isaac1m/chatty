import { Avatar } from "@mui/material";
import React, { useEffect, useState } from "react";
import db, { addData } from "../../config/firebase";
import { Link } from "react-router-dom";
import { doc, getDocs, collection } from "firebase/firestore";

import "./SidebarChat.css";

const SidebarChat = ({ addNewChatRoom, chatRoom }) => {
  const [seed, setSeed] = useState("");
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    setSeed(Math.floor(Math.random() * 1200));
  }, []);

  useEffect(() => {
    async function fetchAndSetMessages() {
      const messagesArr = [];
      const { id } = chatRoom;
      if (chatRoom.id) {
        const docRef = doc(db, "chat-rooms", id);

        const messagesSnapshot = await getDocs(collection(docRef, "messages"));

        messagesSnapshot?.forEach((doc) => {
          messagesArr.push(doc.data());
        });

        setMessages(messagesArr);

        console.log(messages);
      }
    }

    fetchAndSetMessages();
  }, [chatRoom]);

  const createRoom = () => {
    const roomName = prompt("Enter room name");
    if (roomName) {
      addData(db, "chat-rooms", roomName);
    }
  };
  return !addNewChatRoom ? (
    <Link to={`rooms/${chatRoom.id}`}>
      <div className="sidebarChat">
        <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
        <div className="sidebarChat__info">
          <h2>{chatRoom.data.name}</h2>
          <p>{messages[0]?.message}</p>
        </div>
      </div>
    </Link>
  ) : (
    <div className="sidebarChat" onClick={createRoom}>
      <h2>Add New Chat</h2>
    </div>
  );
};

export default SidebarChat;
